import Vue              from 'vue'
import Router           from 'vue-router'
import Home             from '../components/HomeComponent'

/*import StudentAlta      from '../views/Students/Alta'
import StudentBaja      from '../views/Students/Baja'
import StudentConsulta  from '../views/Students/Consulta'
import StudentModificar from '../views/Students/Modificar'*/

import Student          from '../views/Students'

import Group        from '../views/Groups'

import Subject          from '../views/Subjects'
import Period           from '../views/Periods'

import Inscription      from '../views/Inscription'
import Reinscription    from '../views/Reinscription'
import PaymentColegiatura from '../views/Payments/Colegiatura'

import ReportTuition    from '../views/Reports/Tuition'
import ReportInscription    from '../views/Reports/Inscription'
import ReportReinscription  from '../views/Reports/Reinscription'

import Qualification    from '../views/Qualifications'
import CaptureQualification    from '../views/CaptureQualification'
import Kardex           from '../views/Kardex'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/estudiantes',
            component: Student
        },
        /*{
            path: '/estudiantes/alta',
            component: StudentAlta
        },
        {
            path: '/estudiantes/baja',
            component: StudentBaja
        },
        {
            path: '/estudiantes/consulta',
            component: StudentConsulta
        },
        {
            path: '/estudiantes/modificar',
            component: StudentModificar
        },*/
        {
            path: '/estudiantes/kardex/:id',
            component: Kardex,
            props: true
        },
        {
            path: '/pagos/colegiatura',
            component: PaymentColegiatura
        },
        {
            path: '/grupos',
            component: Group
        },
        {
            path: '/asignaturas',
            component: Subject
        },
        {
            path: '/periodos',
            component: Period
        },
        {
            path: '/pagos/inscripcion',
            component: Inscription
        },
        {
            path: '/pagos/reinscripcion',
            component: Reinscription
        },
        {
            path: '/reportes/colegiatura',
            component: ReportTuition
        },
        {
            path: '/reportes/inscripcion',
            component: ReportInscription
        },
        {
            path: '/reportes/reinscripcion',
            component: ReportReinscription
        },
        {
            path: '/calificaciones',
            component: Qualification
        },
        {
            path: '/calificaciones/:group',
            component: CaptureQualification,
            props: true
        }
    ]
});
