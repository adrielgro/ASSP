window.EventBus = new Vue();

import Vue      from 'vue'
import router   from './router'
import Vuetify  from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify);

Vue.component('example-component', require('./components/HomeComponent.vue').default);
Vue.component('navbar-component', require('./components/NavbarComponent').default);
Vue.component('snackbar-component', require('./components/SnackbarComponent.vue').default);

new Vue({
    el: '#app',
    router
});
