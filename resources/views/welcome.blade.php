<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ASSP</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <v-app>
                <navbar-component></navbar-component>
                <v-content>
                    <v-container>
                        <router-view></router-view>
                    </v-container>
                </v-content>
                <snackbar-component></snackbar-component>
            </v-app>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
