<?php

namespace App\Http\Controllers;

use App\Inscription;
use App\Student;
use App\Period;
use Illuminate\Http\Request;

class InscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inscriptions = Inscription::all();
        foreach($inscriptions as $inscription) {
            $student = Student::where('id', $inscription->student_id)->first();
            $inscription->student = $student->name." ".$student->lastname_first." ".$student->lastname_second;

            $period = Period::where('id', $inscription->period_id)->first();
            $inscription->period = $period->name;
        }
        return $inscriptions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::where('id', $request->student)->first();
        if($student === null) {
            return response()->json("No se encontró la matrícula", 500);
        } else {
            $issetInscription = Inscription::where(['student_id' => $student->id])->first();
            if($issetInscription !== null) {
                return response()->json("El estudiante ya tiene una inscripción activa.", 500);
            }
        }

        // Realizamos el pago de la inscripción
        $inscription = new Inscription;
        $inscription->amount = $request->amount;
        $inscription->student_id = $student->id;
        $inscription->period_id = $request->period;
        $inscription->save();
        
        // Activamos al estudiante y le asignamos su periodo
        $student = Student::where('id', $request->student)->first();
        $student->active = true;
        $student->group_id = $request->group;
        $student->period_id = $request->period;
        $student->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inscription  $inscription
     * @return \Illuminate\Http\Response
     */
    public function show(Inscription $inscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inscription  $inscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inscription $inscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inscription  $inscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inscription $inscription)
    {
        //
    }
}
