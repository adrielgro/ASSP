<?php

namespace App\Http\Controllers;

use App\Tuition;
use App\Student;
use App\Period;
use Illuminate\Http\Request;

class TuitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tuitions = Tuition::all();
        foreach($tuitions as $tuition) {
            $student = Student::where('id', $tuition->student_id)->first();
            $tuition->student = $student->name." ".$student->lastname_first." ".$student->lastname_second;

            $period = Period::where('id', $tuition->period_id)->first();
            $tuition->period = $period->name;
        }

        return $tuitions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::where('id', $request->enrollment)->first();
        if($student == null) return response()->json("Matrícula no existente.", 500);
        if(!$student->active) return response()->json("El estudiante aún no está inscrito.", 500);

        $tuition = new Tuition;
        $tuition->modality = "monthly";
        $tuition->month = $request->month;
        $tuition->amount = $request->amount;
        $tuition->period_id = $request->period;
        $tuition->student_id = $student->student;
        $tuition->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tuition  $tuition
     * @return \Illuminate\Http\Response
     */
    public function show(Tuition $tuition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tuition  $tuition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tuition $tuition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tuition  $tuition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tuition $tuition)
    {
        //
    }
}
