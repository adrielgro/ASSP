<?php

namespace App\Http\Controllers;

use App\Reinscription;
use App\Inscription;
use App\Student;
use App\Period;
use Illuminate\Http\Request;

class ReinscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reinscriptions = Reinscription::all();
        foreach($reinscriptions as $reinscription) {
            $student = Student::where('id', $reinscription->student_id)->first();
            $reinscription->student = $student->name." ".$student->lastname_first." ".$student->lastname_second;

            $period = Period::where('id', $reinscription->period_id)->first();
            $reinscription->period = $period->name;
        }
        return $reinscriptions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::where('id', $request->student)->first();
        if($student === null) {
            return response()->json("No se encontró la matrícula", 500);
        } else {
            $issetInscription = Inscription::where(['student_id' => $student->id])->first();
            if($issetInscription === null) {
                return response()->json("El estudiante aún no tiene una inscripción activa.", 500);
            }

            $issetReinscription = Reinscription::where(['student_id' => $student->id, 'period_id' => $request->period])->first();
            if($issetReinscription !== null) {
                return response()->json("El estudiante ya se encuentra reinscrito en este periodo.", 500);
            }
        }

        // Realizamos el pago de la reinscripción
        $reinscription = new Reinscription;
        $reinscription->amount = $request->amount;
        $reinscription->student_id = $student->id;
        $reinscription->period_id = $request->period;
        $reinscription->save();
        
        // Asignamos el nuevo periodo y grupo al alumno
        $student = Student::where('id', $request->student)->first();
        $student->group_id = $request->group;
        $student->period_id = $request->period;
        $student->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reinscription  $reinscription
     * @return \Illuminate\Http\Response
     */
    public function show(Reinscription $reinscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reinscription  $reinscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reinscription $reinscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reinscription  $reinscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reinscription $reinscription)
    {
        //
    }
}
