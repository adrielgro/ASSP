<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Group;
use App\GroupDetail;
use App\Subject;
use App\Student;
use App\Period;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        foreach($groups as $group) {
            $details = GroupDetail::where('group_id', $group->id)->get();
            $subjects_list = [];
            $subjects_name = [];
            foreach($details as $det) {
                $subject = Subject::where('id', $det->subject_id)->select('id', 'name')->first();
                array_push($subjects_list, $subject->id);
                array_push($subjects_name, $subject->name);
            }
            $group->subjects = $subjects_list;
            $group->subjects_name = $subjects_name;
        }

        return $groups;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new Group;
        $group->name = $request->name;
        $group->save();

        $subjects_list = [];
        $subjects_name = [];
        if($request->subjects) {
            foreach($request->subjects as $subject_id) {
                $det = new GroupDetail;
                $det->group_id = $group->id;
                $det->subject_id = $subject_id;
                $det->save();

                $subject = Subject::where('id', $subject_id)->first();
                array_push($subjects_list, $subject->id);
                array_push($subjects_name, $subject->name);
            }
        }

        return response()->json(['group_id' => $group->id, 'subjects' => $subjects_list, 'subjects_name' => $subjects_name], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::where('id', $id)->firstOrFail();
        $group->name = $request->name;
        $group->save();

        if($request->subjects) {

            // Agregar asignaturas nuevas
            foreach($request->subjects as $subject_id) {
                $det = GroupDetail::where(['group_id' => $group->id, 'subject_id' => $subject_id])->first();
                if($det === null)  {
                    $newDet = new GroupDetail;
                    $newDet->group_id = $group->id;
                    $newDet->subject_id = $subject_id;
                    $newDet->save();
                }
            }

            // Eliminar asignaturas removidas
            $currentSubjects = GroupDetail::where(['group_id' => $group->id])->get();
            foreach($currentSubjects as $currentSubject) {
                $issetSubject = false;
                foreach($request->subjects as $newSubject_id) {
                    if($currentSubject->subject_id == $newSubject_id) {
                        $issetSubject = true;
                        break;
                    }
                }
                if(!$issetSubject) {
                    // Eliminamos la materia del grupo
                    $deletedSubject = GroupDetail::find($currentSubject->id);
                    $deletedSubject->delete();
                }
            }
        } else {
            // Eliminar todas las asignaturas
            $currentSubjects = GroupDetail::where(['group_id' => $group->id])->get();
            foreach($currentSubjects as $currentSubject) {
                $deletedSubject = GroupDetail::find($currentSubject->id);
                $deletedSubject->delete();
            }
        }

        // Nombres de las asignaturas
        $subjects = [];
        $details = GroupDetail::where(['group_id' => $group->id])->get();
        foreach($details as $det) {
            $subject = Subject::where('id', $det->subject_id)->first();
            array_push($subjects, $subject->name);
        }

        return response()->json($subjects, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::where('id', $id)->firstOrFail();
        $group->delete();

        return response()->json("Delete", 200);
    }

    public function subjects($group_id)
    {
        $group = Group::where('id', $group_id)->first();
        $groupDetails = GroupDetail::where('group_id', $group->id)->get();
        foreach($groupDetails as $det) {
            $subject = Subject::where('id', $det->subject_id)->first();
            $det->subject = $subject->name;
            $det->subject_id = $subject->id;
        }

        return $groupDetails;
    }

    public function students($group_id)
    {
        $currentPeriod = Period::where('active', 1)->first();
        $students = Student::where(['group_id' => $group_id, 'period_id' => $currentPeriod->id])->get();

        foreach($students as $student)
            $student->fullname = $student->name." ".$student->lastname_first." ".$student->lastname_second;

        return $students;
    }
}
