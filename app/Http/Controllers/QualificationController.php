<?php

namespace App\Http\Controllers;

use App\Qualification;
use App\Period;
use Illuminate\Http\Request;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentPeriod = Period::where('active', 1)->first();

        $issetQualification = Qualification::where(['partial' => $request->partial, 'group_id' => $request->group, 'subject_id' => $request->subject, 'period_id' => $currentPeriod->id, 'student_id' => $request->student])->first();
        if($issetQualification !== null) {
            return response()->json("Ya ha sido capturada la calificación en este parcial.", 500);
        }

        $qualification = new Qualification;
        $qualification->note = $request->note;
        $qualification->partial = $request->partial;
        $qualification->group_id = $request->group;
        $qualification->subject_id = $request->subject;
        $qualification->period_id = $currentPeriod->id;
        $qualification->student_id = $request->student;
        $qualification->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function show(Qualification $qualification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Qualification $qualification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Qualification $qualification)
    {
        //
    }
}
