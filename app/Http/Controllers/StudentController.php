<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;
use App\Group;
use App\Period;
use DB;
use Carbon\Carbon;
use App\User;
use App\Http\Requests\StudentRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = [];
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $students = Student::join('users', 'students.id', '=', 'users.student_id')->get()->makeHidden('password');
            foreach($students as $student) {
                $group = Group::where('id', $student->group_id)->first();
                if($group !== null)
                    $student->group = $group->name;
                else
                    $student->group = "Ninguno";

                $period = Period::where('id', $student->period_id)->first();
                if($period !== null)
                    $student->period = $period->name;
                else
                    $student->period = "Ninguno";
            }
        }
        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $student = new Student;
            $student->name                  = $request->name;
            $student->lastname_first        = ($request->lastname_first) ? $request->lastname_first : "";
            $student->lastname_second       = ($request->lastname_second) ? $request->lastname_second : "";
            $student->enrollment            = $request->enrollment;
            
            $student->curp                  = $request->curp;
            $student->phone                 = $request->phone;
            $student->school_origin         = $request->origin_school;
            $student->school_origin_average = $request->school_origin_average;
            
            $student->birthdate             = ($request->birthdate) ? Carbon::createFromFormat('d/m/Y', $request->birthdate) : null;
            $student->birthplace            = $request->brithplace;
            $student->blood                 = $request->blood_type;
            $student->gender                = $request->gender;
            
            $student->address_street        = $request->address_street;
            $student->address_number        = $request->address_number;
            $student->address_colony        = $request->address_colony;
            $student->address_zip           = $request->address_zip;
            $student->address_phone         = $request->address_phone;
            $student->address_roomiename    = $request->address_roomiename;

            $student->tutor_name            = $request->name;
            $student->tutor_occupation      = $request->tutor_occupation;

            $student->missing_tutor_fullname_1      = $request->missing_tutor_fullname_1;
            $student->missing_tutor_relationship_1  = $request->missing_tutor_relationship_1;
            $student->missing_tutor_phone_1         = $request->missing_tutor_phone_1;
            $student->missing_tutor_fullname_2      = $request->missing_tutor_fullname_2;
            $student->missing_tutor_relationship_2  = $request->missing_tutor_relationship_2;
            $student->missing_tutor_phone_2         = $request->missing_tutor_phone_2;

            $student->birth_certificate_original        = $request->birth_certificate_original;
            $student->highschool_certificate_original   = $request->highschool_certificate_original;
            $student->curp_original                     = $request->curp_original;
            $student->partial_certificate_original      = $request->partial_certificate_original;
            $student->academic_history_original         = $request->academic_history_original;
            $student->proof_address_original            = $request->proof_address_original;
            $student->child_photography_original        = $request->child_photography_original;
            $student->good_conduct_original             = $request->good_conduct_original;
            $student->social_security_original          = $request->social_security_original;
            $student->antidoping_original               = $request->antidoping_original;
            $student->equivalence_payment_original      = $request->equivalence_payment_original;

            $student->signed_regulation = $request->regulation;
            
            if($student->save()) {
                User::create([
                    'name'          => $request->name,
                    'email'         => $request->email,
                    'password'      => ($request->password) ? bcrypt($request->password) : null,
                    'student_id'    => $student->id
                ]);
    
                return $student->id;
            } else {
                return response()->json("Ocurrió un error", 500);
            }

            
        } else {
            return response()->json("No tienes permisos", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $student = Student::where('id', $id)->firstOrFail();

            return response()->json($student, 200);
        } else {
            return response()->json("No tienes permisos", 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $student = Student::where('id', $id)->firstOrFail();
            $student->name = $request->name;
            $student->lastname_first = $request->lastname_first;
            $student->lastname_second = $request->lastname_second;
            $student->phone = $request->phone;
            $student->address = $request->address;
            $student->save();
        } else {
            return response()->json("No tienes permisos", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $student = Student::find($id);
            $student->delete();
        } else {
            return response()->json("No tienes permisos", 500);
        }
    }

    /*public function destroyWithName(Request $request)
    {
        $student = Student::where(['name' => $request->name, 'lastname_first' => $request->lastname_first, 'lastname_second' => $request->lastname_second])->firstOrFail();
        $student->active = false;
        $student->save();
    }*/

    public function search($keyword)
    {
        if(auth()->user()->hasRole('admin') || 
            auth()->user()->hasRole('direccion') || 
            auth()->user()->hasRole('escolares')
        ) {
            $result = Student::where(DB::raw("CONCAT_WS(`name`, ' ', `lastname_first`, ' ', `lastname_second`, ' #', `id`)"), 'LIKE', "%".$keyword."%")->get();
            return response()->json(["count" => count($result), "entries" => $result], 200);
        } else {
            return response()->json("No tienes permisos", 500);
        }
        
    }
}
