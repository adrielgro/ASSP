<?php

namespace App\Http\Controllers;

use App\Period;
use Illuminate\Http\Request;

class PeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Period::where('active', 1)->get();
    }

    public function all()
    {
        return Period::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $period = new Period;
        $period->name = $request->name;
        $period->save();

        return response()->json($period->id, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function show(Period $period)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->active == 1) {
            $issetPeriod = Period::where([
                ['active', '=', 1],
                ['id', '<>', $id]
            ])->first();
            if($issetPeriod !== null)
                return response()->json("Ya existe un periodo activo", 500);
        }
        $period = Period::find($id);
        $period->name = $request->name;
        $period->active = $request->active;
        $period->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $period = Period::find($id);
        $period->delete();
    }
}
