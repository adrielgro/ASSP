<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;
use App\Qualification;
use App\Period;
use App\Group;
use App\GroupDetail;
use App\Subject;

class KardexController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where('id', $id)->first();
        $periods = Period::all();
        $subjects = Subject::all();
        foreach($periods as $period) {
            $subjects_list = [];
            foreach($subjects as $subject) {
                $qualifications = Qualification::where(['period_id' => $period->id, 'subject_id' => $subject->id, 'student_id' => $student->id])->get();
                
                if(count($qualifications) > 0) {
                    $subject->qualifications = $qualifications;
                    array_push($subjects_list, $subject);
                }
                
            }
            $period->subjects = $subjects_list;
        }

        return $periods;
    }
}
