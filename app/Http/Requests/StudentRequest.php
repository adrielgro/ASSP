<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                              => 'required|max:32',
            'lastname_first'                    => 'max:32|nullable',
            'lastname_second'                   => 'max:32|nullable',
            'enrollment'                        => 'required|unique:students',
            'curp'                              => 'max:18|nullable',
            'phone'                             => 'max:32|nullable',
            'origin_school'                     => 'max:64|nullable',
            'school_origin_average'             => 'max:10|nullable',
            
            //'birthdate'                         => 'date|nullable',
            'brithplace'                        => 'max:10|nullable',
            'blood_type'                        => 'in:O negative,O positive,A negative,A positive,B negative,B positive,AB negative,AB positive,other|nullable',
            'gender'                            => 'in:male,female',

            'email'                             => 'required|email|unique:users|max:64',
            'password'                          => 'max:32|min:4|nullable',
            
            'address_street'                    => 'max:64|nullable',
            'address_number'                    => 'max:16|nullable',
            'address_colony'                    => 'max:64|nullable',
            'address_zip'                       => 'max:16|nullable',
            'address_phone'                     => 'max:16|nullable',
            'address_roomiename'                => 'max:64|nullable',

            'tutor_name'                        => 'max:64|nullable',
            'tutor_occupation'                  => 'max:64|nullable',

            'missing_tutor_fullname_1'          => 'max:64|nullable',
            'missing_tutor_relationship_1'      => 'max:64|nullable',
            'missing_tutor_phone_1'             => 'max:16|nullable',
            'missing_tutor_fullname_2'          => 'max:64|nullable',
            'missing_tutor_relationship_2'      => 'max:64|nullable',
            'missing_tutor_phone_2'             => 'max:16|nullable',

            'birth_certificate_original'        => 'boolean',
            'highschool_certificate_original'   => 'boolean',
            'curp_original'                     => 'boolean',
            'partial_certificate_original'      => 'boolean',
            'academic_history_original'         => 'boolean',
            'proof_address_original'            => 'boolean',
            'child_photography_original'        => 'boolean',
            'good_conduct_original'             => 'boolean',
            'social_security_original'          => 'boolean',
            'antidoping_original'               => 'boolean',
            'equivalence_payment_original'      => 'boolean',

            'regulation'                        => 'in:yes,no|required',
        ];
    }
}
