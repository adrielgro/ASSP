<?php
Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('api/students', 'StudentController@index');
    Route::post('api/students', 'StudentController@store');
    Route::post('api/students/update/{id}', 'StudentController@update');
    Route::post('api/students/delete/{id}', 'StudentController@destroy');
    Route::get('api/students/search/{keyword}', 'StudentController@search');

    Route::get('api/subjects', 'SubjectController@index');
    Route::post('api/subjects', 'SubjectController@store');
    Route::post('api/subjects/update/{id}', 'SubjectController@update');
    Route::post('api/subjects/delete/{id}', 'SubjectController@destroy');

    Route::get('api/groups', 'GroupController@index');
    Route::post('api/groups', 'GroupController@store');
    Route::post('api/groups/update/{id}', 'GroupController@update');
    Route::post('api/groups/delete/{id}', 'GroupController@destroy');
    Route::get('api/groups/subjects/{group}', 'GroupController@subjects');
    Route::get('api/groups/students/{group}', 'GroupController@students');

    Route::get('api/kardex/{id}', 'KardexController@show');

    Route::get('api/periods', 'PeriodController@index');
    Route::get('api/periods/all', 'PeriodController@all');
    Route::post('api/periods', 'PeriodController@store');
    Route::post('api/periods/update/{id}', 'PeriodController@update');
    Route::post('api/periods/delete/{id}', 'PeriodController@destroy');

    Route::get('api/inscriptions', 'InscriptionController@index');
    Route::post('api/inscriptions', 'InscriptionController@store');

    Route::get('api/reinscriptions', 'ReinscriptionController@index');
    Route::post('api/reinscriptions', 'ReinscriptionController@store');

    Route::get('api/tuitions', 'TuitionController@index');
    Route::post('api/tuition', 'TuitionController@store'); // Colegiatura

    Route::post('api/qualifications', 'QualificationController@store');

    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });
});