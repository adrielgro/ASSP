<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('enrollment')->nullable()->unique();
            $table->string('name');
            $table->string('lastname_first')->nullable();
            $table->string('lastname_second')->nullable();
            $table->enum('shift', ['morning', 'afternoon'])->nullable();
            $table->string('curp')->nullable();
            $table->string('phone')->nullable();
            $table->string('school_origin')->nullable();
            $table->string('school_origin_average')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('birthplace')->nullable();
            $table->enum('blood', ['O negative', 'O positive', 'A negative', 'A positive', 'B negative', 'B positive', 'AB negative', 'AB positive', 'other'])->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            
            $table->string('address_street')->nullable();
            $table->string('address_number')->nullable();
            $table->string('address_colony')->nullable();
            $table->string('address_zip')->nullable();
            $table->string('address_roomiename')->nullable();
            $table->string('address_phone')->nullable();

            $table->string('tutor_name')->nullable();
            $table->string('tutor_occupation')->nullable();

            $table->string('missing_tutor_fullname_1')->nullable();
            $table->string('missing_tutor_relationship_1')->nullable();
            $table->string('missing_tutor_phone_1')->nullable();
            $table->string('missing_tutor_fullname_2')->nullable();
            $table->string('missing_tutor_relationship_2')->nullable();
            $table->string('missing_tutor_phone_2')->nullable();

            $table->boolean('birth_certificate_original')->default(false);
            $table->boolean('highschool_certificate_original')->default(false);
            $table->boolean('curp_original')->default(false);
            $table->boolean('partial_certificate_original')->default(false);
            $table->boolean('academic_history_original')->default(false);
            $table->boolean('proof_address_original')->default(false);
            $table->boolean('child_photography_original')->default(false);
            $table->boolean('good_conduct_original')->default(false);
            $table->boolean('social_security_original')->default(false);
            $table->boolean('antidoping_original')->default(false);
            $table->boolean('equivalence_payment_original')->default(false);

            $table->enum('signed_regulation', ['yes', 'no']);

            $table->boolean('active')->default(false);
            $table->bigInteger('group_id')->nullable()->unsigned();
            $table->bigInteger('period_id')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
