<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'      => 'Admin',
            'slug'      => 'admin',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Dirección',
            'slug'      => 'direccion',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Escolares',
            'slug'      => 'escolares',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Prefectura',
            'slug'      => 'prefectura',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Psicología',
            'slug'      => 'psicologia',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Contabilidad',
            'slug'      => 'contabilidad',
            'special'   => 'all-access'
        ]);

        Role::create([
            'name'      => 'Docentes',
            'slug'      => 'docentes',
            'special'   => 'all-access'
        ]);
    }
}
