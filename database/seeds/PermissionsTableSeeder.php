<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'Navegar alumnos',
            'slug' => 'students.index',
            'description' => 'Lista y navega en los alumnos'
        ]);

        Permission::create([
            'name' => 'Añadir alumnos',
            'slug' => 'students.create',
            'description' => 'añadir nuevos alumnos'
        ]);

        Permission::create([
            'name' => 'Edición de alumnos',
            'slug' => 'students.edit',
            'description' => 'Edita cualquier alumno'
        ]);

        Permission::create([
            'name' => 'Eliminar alumnos',
            'slug' => 'students.destroy',
            'description' => 'Elimina cualquier alumno'
        ]);

        /*Permission::create([
            'name' => 'Asignaturas',
            'slug' => 'subjects.*',
            'description' => 'Acceso completo al módulo de asignaturas.'
        ]);

        Permission::create([
            'name' => 'Grupos',
            'slug' => 'groups.*',
            'description' => 'Acceso completo al módulo de grupos.'
        ]);

        Permission::create([
            'name' => 'Periodos',
            'slug' => 'periods.*',
            'description' => 'Acceso completo al módulo de periodos.'
        ]);

        Permission::create([
            'name' => 'Pagos',
            'slug' => 'payments.*',
            'description' => 'Acceso completo al módulo de pagos.'
        ]);

        Permission::create([
            'name' => 'Reportes',
            'slug' => 'reports.*',
            'description' => 'Acceso completo al módulo de reportes.'
        ]);

        Permission::create([
            'name' => 'Calificaciones',
            'slug' => 'Qualifications.*',
            'description' => 'Acceso completo al módulo de calificaciones.'
        ]);*/
    }
}
